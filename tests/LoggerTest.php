<?php declare(strict_types=1);

namespace Terah\ColourLog\Test;

use PHPUnit_Framework_TestCase;
use Psr\Log\LogLevel;
use Terah\ColourLog\Logger;

class LoggerTest extends PHPUnit_Framework_TestCase
{
    /** @var Logger */
    public $logger  = null;
    public $logfile = null;

    public function setUp()
    {
        $this->logfile = fopen('php://temp', 'r+');
        $this->logger  = new Logger($this->logfile, LogLevel::DEBUG, false, false, false);
    }

    public function testColourize()
    {
        $colouredStr = $this->logger->colourize('TESTING', 'magento', 'white');
        $this->assertSame('TESTING[0m', $colouredStr);
    }

    public function testSetLogLevelDebug()
    {
        $this->logger->setLogLevel(LogLevel::DEBUG);
        $this->logger->debug('TESTING');
        $this->assertSame("[1;30m[49mDEBUG | TESTING[0m\n", $this->readLastLogLine());
        $this->logfile = fopen('php://temp', 'r+');
        $this->logger->setLogLevel(LogLevel::INFO);
        $this->logger->debug('TESTING');
        $this->assertSame("", $this->readLastLogLine());
    }

    public function testSetLogLevelInfo()
    {
        $this->logger->setLogLevel(LogLevel::INFO);
        $this->logger->info('TESTING');
        $this->assertSame("[0;32m[49mINFO  | TESTING[0m\n", $this->readLastLogLine());
        $this->logfile = fopen('php://temp', 'r+');
        $this->logger->setLogLevel(LogLevel::NOTICE);
        $this->logger->debug('TESTING');
        $this->assertSame("", $this->readLastLogLine());
    }

    public function testSetLogLevelNotice()
    {
        $this->logger->setLogLevel(LogLevel::NOTICE);
        $this->logger->notice('TESTING');
        $this->assertSame("[0;36m[49mNOTE  | TESTING[0m\n", $this->readLastLogLine());
        $this->logfile = fopen('php://temp', 'r+');
        $this->logger->setLogLevel(LogLevel::WARNING);
        $this->logger->debug('TESTING');
        $this->assertSame("", $this->readLastLogLine());
    }

    public function testSetLogLevelWarning()
    {
        $this->logger->setLogLevel(LogLevel::WARNING);
        $this->logger->warning('TESTING');
        $this->assertSame("[1;33m[49mWARN  | TESTING[0m\n", $this->readLastLogLine());
        $this->logfile = fopen('php://temp', 'r+');
        $this->logger->setLogLevel(LogLevel::ERROR);
        $this->logger->debug('TESTING');
        $this->assertSame("", $this->readLastLogLine());
    }

    public function testSetLogLevelError()
    {
        $this->logger->setLogLevel(LogLevel::ERROR);
        $this->logger->error('TESTING');
        $this->assertSame("[0;31m[49mERROR | TESTING[0m\n", $this->readLastLogLine());
        $this->logfile = fopen('php://temp', 'r+');
        $this->logger->setLogLevel(LogLevel::CRITICAL);
        $this->logger->debug('TESTING');
        $this->assertSame("", $this->readLastLogLine());
    }

    public function testSetLogLevelCritical()
    {
        $this->logger->setLogLevel(LogLevel::CRITICAL);
        $this->logger->critical('TESTING');
        $this->assertSame("[0;31m[49mCRIT  | TESTING[0m\n", $this->readLastLogLine());
        $this->logfile = fopen('php://temp', 'r+');
        $this->logger->setLogLevel(LogLevel::EMERGENCY);
        $this->logger->debug('TESTING');
        $this->assertSame("", $this->readLastLogLine());
    }

    public function testSetLogLevelEmergency()
    {
        $this->logger->setLogLevel(LogLevel::EMERGENCY);
        $this->logger->emergency('TESTING');
        $this->assertSame("[1;37m[41mEMERG | TESTING[0m\n", $this->readLastLogLine());
    }

    public function testGzipped()
    {
        $this->logger->gzipped();
    }

    public function testLog()
    {
        $this->logger->log(LogLevel::DEBUG, 'TESTING');
        $output = $this->readLastLogLine();
        $this->assertSame("[1;30m[49mDEBUG | TESTING[0m\n", $output);
    }


    public function testWrite()
    {
        $this->logger->write('TESTING');
        $output = $this->readLastLogLine();
        $this->assertEquals('TESTING', $output);
    }

    protected function readLastLogLine()
    {
        rewind($this->logfile);

        return (string)stream_get_contents($this->logfile);
    }


}
