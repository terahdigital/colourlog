<?php declare(strict_types=1);

namespace Terah\ColourLog;

use Psr\Log\LoggerInterface;

/**
 * Basic Implementation of LoggerAwareInterface.
 */
trait LoggerTrait
{
    protected LoggerInterface $logger;

    /**
     * @param LoggerInterface $logger
     * @return $this
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger           = $logger;

        return $this;
    }


    public function getLogger() : LoggerInterface
    {
        return $this->logger;
    }
}
