<?php declare(strict_types=1);

namespace Terah\ColourLog;

use Closure;
use Exception;
use InvalidArgumentException;
use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * Class Logger
 *
 * @package Terah\ColourLog
 */
class Logger extends AbstractLogger
{
    const BLACK         = 'black';
    const DARK_GRAY     = 'dark_gray';
    const BLUE          = 'blue';
    const LIGHT_BLUE    = 'light_blue';
    const GREEN         = 'green';
    const LIGHT_GREEN   = 'light_green';
    const CYAN          = 'cyan';
    const LIGHT_CYAN    = 'light_cyan';
    const RED           = 'red';
    const LIGHT_RED     = 'light_red';
    const PURPLE        = 'purple';
    const LIGHT_PURPLE  = 'light_purple';
    const BROWN         = 'brown';
    const YELLOW        = 'yellow';
    const MAGENTA       = 'magenta';
    const LIGHT_GRAY    = 'light_gray';
    const WHITE         = 'white';
    const DEFAULT       = 'default';
    const BOLD          = 'bold';

    /**  @var resource $resource The file handle */
    protected $resource         = null;

    /** @var string $level */
    protected $level            = LogLevel::INFO;

    /** @var bool $closeLocally */
    protected $closeLocally     = false;

    /** @var bool */
    protected $addDate          = true;

    /** @var string  */
    protected $separator        = ' | ';

    /** @var Closure */
    protected $formatter        = null;

    /** @var string  */
    protected $lastLogEntry     = '';

    /** @var bool|null  */
    protected $gzipFile         = null;

    /** @var bool  */
    protected $useLocking       = false;

    /**
     * @var array $logLevels List of supported levels
     */
    static protected $logLevels       = [
        LogLevel::EMERGENCY => [1, self::WHITE,       self::RED,      self::DEFAULT,  'EMERG'],
        LogLevel::ALERT     => [2, self::WHITE,       self::YELLOW,   self::DEFAULT,  'ALERT'],
        LogLevel::CRITICAL  => [3, self::RED,         self::DEFAULT,  self::BOLD ,    'CRIT'],
        LogLevel::ERROR     => [4, self::RED,         self::DEFAULT,  self::DEFAULT,  'ERROR'],
        LogLevel::WARNING   => [5, self::YELLOW,      self::DEFAULT,  self::DEFAULT,  'WARN'],
        LogLevel::NOTICE    => [6, self::CYAN,        self::DEFAULT,  self::DEFAULT,  'NOTE'],
        LogLevel::INFO      => [7, self::GREEN,       self::DEFAULT,  self::DEFAULT,  'INFO'],
        LogLevel::DEBUG     => [8, self::LIGHT_GRAY,  self::DEFAULT,  self::DEFAULT,  'DEBUG'],
    ];

    /**
     * @var array
     */
    static protected $colours   = [
        'fore' => [
            self::BLACK         => '0;30',
            self::DARK_GRAY     => '1;30',
            self::BLUE          => '0;34',
            self::LIGHT_BLUE    => '1;34',
            self::GREEN         => '0;32',
            self::LIGHT_GREEN   => '1;32',
            self::CYAN          => '0;36',
            self::LIGHT_CYAN    => '1;36',
            self::RED           => '0;31',
            self::LIGHT_RED     => '1;31',
            self::PURPLE        => '0;35',
            self::LIGHT_PURPLE  => '1;35',
            self::BROWN         => '0;33',
            self::YELLOW        => '1;33',
            self::MAGENTA       => '0;35',
            self::LIGHT_GRAY    => '0;37',
            self::WHITE         => '1;37',
        ],
        'back'  => [
            self::DEFAULT       => '49',
            self::BLACK         => '40',
            self::RED           => '41',
            self::GREEN         => '42',
            self::YELLOW        => '43',
            self::BLUE          => '44',
            self::MAGENTA       => '45',
            self::CYAN          => '46',
            self::LIGHT_GRAY    => '47',
        ],
        self::BOLD => [],
    ];

    /**
     * @param mixed  $resource
     * @param string $level
     * @param bool   $useLocking
     * @param bool   $gzipFile
     * @param bool   $addDate
     */
    public function __construct($resource, string $level=LogLevel::INFO, bool $useLocking=false, bool $gzipFile=false, bool $addDate=true)
    {
        $this->resource     = $resource;
        $this->setLogLevel($level);
        $this->useLocking   = $useLocking;
        $this->gzipFile     = $gzipFile;
        $this->addDate      = $addDate;
    }

    /**
     * @param $resource
     * @return LoggerInterface
     */
    public function setLogFile($resource) : LoggerInterface
    {
        $this->resource     = $resource;

        return $this;
    }

    /**
     * @param string $string
     * @param string $foregroundColor
     * @param string $backgroundColor
     * @param bool $bold
     * @return string
     */
    public static function addColour(string $string, string $foregroundColor='', string $backgroundColor='', bool $bold=false) : string
    {
        // todo: support bold
        unset($bold);
        $coloredString = '';
        // Check if given foreground color found
        if ( isset(static::$colours['fore'][$foregroundColor]) )
        {
            $coloredString .= "\033[" . static::$colours['fore'][$foregroundColor] . "m";
        }
        // Check if given background color found
        if ( isset(static::$colours['back'][$backgroundColor]) )
        {
            $coloredString .= "\033[" . static::$colours['back'][$backgroundColor] . "m";
        }
        // Add string and end coloring
        $coloredString .=  $string . "\033[0m";

        return $coloredString;
    }

    /**
     * @param string    $string
     * @param string    $foregroundColor
     * @param string    $backgroundColor
     * @param bool      $bold
     * @return string
     */
    public function colourize(string $string, string $foregroundColor='', string $backgroundColor='', bool $bold=false) : string
    {
        return static::addColour($string, $foregroundColor, $backgroundColor, $bold);
    }

    /**
     * @param string $level Ignore logging attempts at a level less the $level
     * @return LoggerInterface
     */
    public function setLogLevel(string $level) : LoggerInterface
    {
        if ( ! isset(static::$logLevels[$level]) )
        {
            throw new InvalidArgumentException("Log level is invalid");
        }
        $this->level = static::$logLevels[$level][0];

        return $this;
    }

    /**
     * @return LoggerInterface
     */
    public function lock() : LoggerInterface
    {
        $this->useLocking = true;

        return $this;
    }

    /**
     * @return LoggerInterface
     */
    public function gzipped() : LoggerInterface
    {
        $this->gzipFile = true;

        return $this;
    }

    /**
     * @param callable $fnFormatter
     *
     * @return LoggerInterface
     */
    public function formatter(callable $fnFormatter) : LoggerInterface
    {
        $this->formatter = $fnFormatter;

        return $this;
    }

    /**
     * Log messages to resource
     *
     * @param mixed          $level    The level of the log message
     * @param string|object  $message  If an object is passed it must implement __toString()
     * @param array          $context  Placeholders to be substituted in the message
     *
     * @return LoggerInterface
     */
    public function log($level, $message, array $context=[]) : LoggerInterface
    {
        $level = isset(static::$logLevels[$level]) ? $level : LogLevel::INFO;
        list($logLevel, $fore, $back, $style) = static::$logLevels[$level];
        unset($style);
        if ( $logLevel > $this->level )
        {
            return $this;
        }
        if ( is_callable($this->formatter) )
        {
            $message = $this->formatter->__invoke(static::$logLevels[$level][4], $message, $context);
        }
        else
        {
            $message = $this->formatMessage($level, $message, $context);
        }
        $this->lastLogEntry = $message;
        $this->write($this->colourize($message, $fore, $back) . PHP_EOL);

        return $this;
    }

    /**
     * @param string $style
     * @param string $message
     * @return string
     */
    public static function style(string $style, string $message) : string
    {
        $style = isset(static::$logLevels[$style]) ? $style : LogLevel::INFO;
        list($logLevel, $fore, $back, $style) = static::$logLevels[$style];
        unset($logLevel, $style);

        return static::addColour($message, $fore, $back);
    }

    /**
     * @param string $level
     * @param string $message
     * @param array  $context
     * @return string
     */
    protected function formatMessage(string $level, string $message, array $context=[]) : string
    {
        # Handle objects implementing __toString
        $message            = (string) $message;
        $message            .= empty($context) ? '' : PHP_EOL . json_encode($context, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        $data               = $this->addDate ? ['date' => date('Y-m-d H:i:s')] : [];
        $data['level']      = strtoupper(str_pad(static::$logLevels[$level][4], 5, ' ', STR_PAD_RIGHT));
        $data['message']    = $message;

        return implode($this->separator, $data);
    }

    /**
     * Write the content to the stream
     *
     * @param  string $content
     */
    public function write(string $content)
    {
        $resource = $this->getResource();
        if ( $this->useLocking )
        {
            flock($resource, LOCK_EX);
        }
        gzwrite($resource, $content);
        if ( $this->useLocking )
        {
            flock($resource, LOCK_UN);
        }
    }

    /**
     * @return mixed|resource
     */
    protected function getResource()
    {
        if ( is_resource($this->resource) )
        {
            return $this->resource;
        }
        $fileName               = $this->resource;
        $this->closeLocally     = true;
        $this->resource         = $this->openResource();
        if ( ! is_resource($this->resource) )
        {
            throw new Exception("The resource ({$fileName}) could not be opened");
        }

        return $this->resource;
    }

    /**
     * @return string
     */
    public function getLastLogEntry() : string
    {
        return $this->lastLogEntry;
    }

    /**
     * @return resource
     */
    protected function openResource()
    {
        if ( $this->gzipFile )
        {
            return gzopen($this->resource, 'a');
        }

        return fopen($this->resource, 'a');
    }

    public function __destruct()
    {
        if ($this->closeLocally)
        {
            gzclose($this->getResource());
        }
    }
}

