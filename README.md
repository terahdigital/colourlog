# colourlog

[![Latest Version](https://img.shields.io/github/release/terah/colourlog.svg?style=flat-square)](https://bitbucket.org/terahdigital/colourlog/releases)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Build Status](https://img.shields.io/travis/terah/colourlog/master.svg?style=flat-square)](https://travis-ci.org/terah/colourlog)
[![Coverage Status](https://img.shields.io/scrutinizer/coverage/g/terah/colourlog.svg?style=flat-square)](https://scrutinizer-ci.com/g/terah/colourlog/code-structure)
[![Quality Score](https://img.shields.io/scrutinizer/g/terah/colourlog.svg?style=flat-square)](https://scrutinizer-ci.com/g/terah/colourlog)
[![Total Downloads](https://img.shields.io/packagist/dt/terah/colourlog.svg?style=flat-square)](https://packagist.org/packages/terah/colourlog)


A simple PSR3 compliant logger with basic colours.

## Install

Via Composer

``` bash
$ composer require terah/colourlog
```

## Usage

``` php
$use_file_locking = false;
$gzip_log = false;

$logger = new \Terah\ColourLog\Logger('/path/to/log', \Psr\Log\LogLevel::INFO, $use_file_locking, $gzip_log);
$logger->error("Testing logger");
```

## Testing

``` bash
$ phpunit
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email terry@terah.com.au instead of using the issue tracker.

## Credits

- [Terry Cullen](https://bitbucket.org/terahdigital)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
